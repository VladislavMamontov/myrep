package servlets;

import services.textEditor;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FileEditor extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String text = req.getParameter("newText");
        String path = req.getSession().getAttribute("path") + "\\" + req.getParameter("fileName");

        textEditor.getInstance().editText(text, path);
        getServletContext().getRequestDispatcher("/catalog").forward(req, resp);
    }
}