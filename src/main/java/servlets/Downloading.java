package servlets;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Downloading extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getSession().getAttribute("path") + "\\" + req.getParameter("name");
        resp.setContentType("application/octet-stream");
        resp.setHeader("Content-Disposition","attachment;filename=" + req.getParameter("name"));
        File file = new File(path);
        FileInputStream in = new FileInputStream(file);
        ServletOutputStream out = resp.getOutputStream();
        int i;
        while ((i = in.read())!=-1){
            out.write(i);
        }
        in.close();
        out.flush();
        out.close();
    }
}
