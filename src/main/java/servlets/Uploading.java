package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

@MultipartConfig
public class Uploading extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Part filePart = req.getPart("file");
        String fileName = filePart.getSubmittedFileName();
        File upload = new File((String)req.getSession().getAttribute("path"), fileName);
        try(InputStream fileContent = filePart.getInputStream()){
            Files.copy(fileContent, upload.toPath());
        }
        getServletContext().getRequestDispatcher("/catalog").forward(req, resp);
    }
}
