package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

public class FileCreator extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("newFile");
        HttpSession session = req.getSession();
        new File(session.getAttribute("path") + "\\" + name).createNewFile();
        getServletContext().getRequestDispatcher("/catalog").forward(req, resp);
    }
}