package servlets;

import DAO.NoteDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CatalogCreator extends HttpServlet {
    static {
        NoteDAO.getInstance().connect();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("root") == null) {
            session.setAttribute("root", "C:\\TestDir");
            session.setAttribute("path", "C:\\TestDir");
        }
        String name = req.getParameter("name");
        String path;
        if (name != null) {
            path = goForward(req, name);
        } else if (req.getParameter("back") != null) {
            path = goBack(req);
        } else {
            path = (String) session.getAttribute("path");
        }
        File file = new File(path);
        List<String> dirs = new ArrayList<String>();
        List<String> textFiles = new ArrayList<String>();
        List<String> otherFiles = new ArrayList<String>();
        Map<String, String> notes = new HashMap<>();

        File[] listFiles = file.listFiles();
        for (File x : listFiles) {
            if (x.isDirectory()) {
                dirs.add(x.getName());
                notes.put(x.getName(), NoteDAO.getInstance().getNoteByName(x.getName()));
            } else if (x.getName().endsWith(".txt")) {
                textFiles.add(x.getName());
                notes.put(x.getName(), NoteDAO.getInstance().getNoteByName(x.getName()));
            } else {
                otherFiles.add(x.getName());
                notes.put(x.getName(), NoteDAO.getInstance().getNoteByName(x.getName()));
            }
        }
        req.setAttribute("dirs", dirs);
        req.setAttribute("textFiles", textFiles);
        req.setAttribute("otherFiles", otherFiles);
        req.setAttribute("notes", notes);

        getServletContext().getRequestDispatcher("/view/catalog.jsp").forward(req, resp);
    }

    private String goForward(HttpServletRequest req, String name) {
        String path;
        if (new File(req.getSession().getAttribute("path") + "\\" + name).isDirectory()) {
            path = req.getSession().getAttribute("path") + "\\" + name;
            req.getSession().setAttribute("path", path);
        } else {
            path = (String) req.getSession().getAttribute("path");
        }
        return path;
    }

    private String goBack(HttpServletRequest req) {
        HttpSession session = req.getSession();
        if (session.getAttribute("root").equals(session.getAttribute("path"))) {
            return (String) session.getAttribute("path");
        } else {
            String path = (String) session.getAttribute("path");
            int i = path.lastIndexOf("\\");
            String newPath = path.substring(0, i);
            session.setAttribute("path", newPath);
            return newPath;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    @Override
    public void destroy() {
        NoteDAO.getInstance().disconnect();
        super.destroy();
    }
}
