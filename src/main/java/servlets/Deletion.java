package servlets;

import DAO.NoteDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;

public class Deletion extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("nameDel");
        HttpSession session = req.getSession();
        new File(session.getAttribute("path") + "\\" + name).delete();
        NoteDAO.getInstance().deleteNote(name);
        getServletContext().getRequestDispatcher("/catalog").forward(req, resp);
    }
}