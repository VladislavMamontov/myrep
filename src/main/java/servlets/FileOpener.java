package servlets;

import services.textReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FileOpener extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String fileName = req.getParameter("name");
        String path =req.getSession().getAttribute("path") + "\\" + fileName;
        String text = textReader.getInstance().getText(path);
        req.setAttribute("text", text);
        req.setAttribute("fileName", fileName);
        getServletContext().getRequestDispatcher("/view/editor.jsp").forward(req, resp);
    }
}