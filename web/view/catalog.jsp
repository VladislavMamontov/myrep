
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Catalog</title>
</head>
<body>
<h2>Catalog:</h2>
<h4>Directories:</h4>
<ul>
    <a href="${pageContext.request.contextPath}/catalog?back=1">. . .</a>
    <br>
    <c:forEach var="dir" items="${dirs}">
        <c:out value="Note: ${notes[dir]}"/><br>
        <img src="images/directory.png" width="16" height="16">
        <a href="${pageContext.request.contextPath}/catalog?name=${dir}">${dir}</a>
        <button onclick="location.href='${pageContext.request.contextPath}/delete?nameDel=${dir}'">Delete</button>
        <form action="${pageContext.request.contextPath}/createNote?nameNote=${dir}" method="post">
            <input name="note"/>
            <input type="submit" value="Add note"/>
        </form>
    </c:forEach>
</ul>
<form action="${pageContext.request.contextPath}/createDir" method="post">
    Create new directory: <input name="newDir"/>
    <input type="submit" value="Create"/>
    <br>
</form>
<h4>Files:</h4>
<ul>
    <c:forEach var="file" items="${textFiles}">
        <c:out value="Note: ${notes[file]}"/><br>
        <img src="images/text.png" width="16" height="16">
        <a href="${pageContext.request.contextPath}/download?name=${file}">${file}</a>
        <button onclick="location.href='${pageContext.request.contextPath}/delete?nameDel=${file}'">Delete</button>
        <button onclick="location.href='${pageContext.request.contextPath}/open?name=${file}'">Edit</button>
        <form action="${pageContext.request.contextPath}/createNote?nameNote=${file}" method="post">
            <input name="note"/>
            <input type="submit" value="Add note"/>
        </form>
    </c:forEach>

    <c:forEach var="file" items="${otherFiles}">
        <c:out value="Note: ${notes[file]}"/><br>
        <a href="${pageContext.request.contextPath}/download?name=${file}">${file}</a>
        <button onclick="location.href='${pageContext.request.contextPath}/delete?nameDel=${file}'">Delete</button>
        <form action="${pageContext.request.contextPath}/createNote?nameNote=${file}" method="post">
            <input name="note"/>
            <input type="submit" value="Add note"/>
        </form>
    </c:forEach>
</ul>
<form action="${pageContext.request.contextPath}/createFile" method="post">
    Create new file: <input name="newFile"/>
    <input type="submit" value="Create"/>
</form>

<form action="${pageContext.request.contextPath}/upload" method="post" enctype="multipart/form-data">
    Download file:
    <input type="file" name="file" />
    <input type="submit" />
</form>
</body>
</html>
